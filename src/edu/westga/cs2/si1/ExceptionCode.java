package edu.westga.cs2.si1;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The class contains code that may throw exceptions
 * @author CS2
 * @version 1.0
 *
 */
public class ExceptionCode {

	/**
	 * The method takes an array list of integers and generates a list of quotients that are created by dividing the integers by 
	 * user supplied numbers.
	 * 
	 * @param values an ArrayList of Integers
	 * @precondition values != null && values.size() !=0
	 * @postcondition none
	 * 
	 */
	public void displayQuotient(ArrayList<Integer> values) {
		
		if (values == null) {
			throw new IllegalArgumentException("list cannot be null");
		}
		if (values.size() == 0) {
			throw new IllegalArgumentException("list cannot be empty");
		}
		try {
			this.generateQuotient(values);
			this.printResults(values);
		} catch (InputMismatchException e) {
			System.err.println("input mismatch" + e.getMessage());
		} catch (Exception e) {
			System.err.println("Houston, we have a problem." + e.getMessage());
		}

		System.out.println("Size of list is: " + values.size());
	}



	private void generateQuotient(ArrayList<Integer> myList) {
		Scanner input = new Scanner(System.in);

		try {
			for (int i = 0; i < myList.size(); i++) {
				try {
					System.out.print("Enter a divisor: ");
					int divideBy = input.nextInt();
					int result = myList.get(i) / divideBy;
					myList.set(i, result);
				} catch (ArithmeticException e) {
					System.err.println("Division by zero.");
				}
			}
		} catch (NullPointerException e) {
			System.err.println("null exception");
			e.printStackTrace();
		} finally {
			input.close();
		}
	}
	
	// add a private printResults method that takes an ArrayList of Integers and prints out all the values in the ArrayList
	// in the format of (number1, number2, ...)
	
}
